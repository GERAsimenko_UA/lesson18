<?php


class MyClass
{
    public $name;
    public $age;
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name . ' ' . $this->age;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}